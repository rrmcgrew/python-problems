#!/usr/bin/env python3
"""Problem #4 from dailycodingproblem.com

Given an array of integers, find the first missing positive integer in linear
time and constant space. In other words, find the lowest positive integer that
does not exist in the array. The array can contain duplicates and negative
numbers as well.

For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should
give 3.

You can modify the input array in-place.
"""


def find_missing(input):
    """Determines the first missing positive integer in the given input list.

    If the end of the list is reached and no integer is missing, the next
    integer is presumed to be missing.

    Args:
        input: The list of integers to be checked.

    Returns:
        The first missing positive integer in the list.
    """
    missing = None
    current = 1

    while not missing:
        missing = None if current in input else current
        current = current + 1

    return missing


list_1 = [3, 4, -1, 1]
list_2 = [1, 2, 0]
list_3 = [-1, -4, -5, 0]

assert find_missing(list_1) == 2
assert find_missing(list_2) == 3
assert find_missing(list_3) == 1
