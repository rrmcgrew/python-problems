#!/usr/bin/env python3
"""Problem #11 from dailycodingproblem.com

Implement an autocomplete system. That is, given a query string s and a set of
all possible query strings, return all strings in the set that have s as a
prefix.

For example, given the query string de and the set of strings
[dog, deer, deal], return [deer, deal].

Hint: Try preprocessing the dictionary into a more efficient data structure to
speed up queries.
"""


class CompletionSuggestion():
    """Provides completion suggestion capabilities."""

    def __init__(self, master):
        self.master = sorted(master)

    def suggestions(self, stub):
        """Provides completion suggestions for the given stub from the
        preconfigured master list of words.

        This function assumes that if the master list is populated that it
        contains no values of `None`.

        Args:
            stub: The leading characters from which to determine completion
                suggestions.

        Returns:
            List of possible words that begin with the given stub. If no word
            is found within the master list, an empty list is returned.
        """
        return [word for word in self.master if word.startswith(stub)]


if __name__ == '__main__':
    master = ['dog', 'deer', 'deal']
    suggestion = CompletionSuggestion(master)

    assert suggestion.suggestions('d') == ['deal', 'deer', 'dog']
    assert suggestion.suggestions('de') == ['deal', 'deer']
    assert suggestion.suggestions('dea') == ['deal']
    assert suggestion.suggestions('deal') == ['deal']
    assert suggestion.suggestions('deer') == ['deer']
    assert suggestion.suggestions('dog') == ['dog']
    assert suggestion.suggestions('something') == []
