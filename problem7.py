#!/usr/bin/env python3
"""Problem #7 from dailycodingproblem.com

Given the mapping a = 1, b = 2, ... z = 26, and an encoded message, count the
number of ways it can be decoded.

For example, the message '111' would give 3, since it could be decoded as:
'aaa', 'ka', and 'ak'.

You can assume that the messages are decodable. For example, '001' is not
allowed.
"""


def possible_combo_count(input):
    """Determines the number of possible decoded combinations for a given
    non-delimited input string. This method assumes only valid inputs can
    be given. This method runs recursively to determine count based on
    multiple outcomes.

    Args:
        input: The string containing the encoded message.

    Returns:
        The number of possible decoded messages.
    """
    possible_combos = None

    if not input:
        possible_combos = 0
    elif len(input) == 1:
        possible_combos = 1
    elif len(input) == 2:
        possible_combos = 2 if int(input) <= 26 else 1
    else:
        possible_combos = possible_combo_count(input[1:])

        if int(input[:2]) <= 26:
            possible_combos += possible_combo_count(input[2:])

    return possible_combos


if __name__ == '__main__':
    assert possible_combo_count('111') == 3
    assert possible_combo_count('27') == 1
    assert possible_combo_count('23') == 2
