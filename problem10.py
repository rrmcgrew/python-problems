#!/usr/bin/env python3
"""Problem #10 from dailycodingproblem.com

Implement a job scheduler which takes in a function f and an integer n, and
calls f after n milliseconds.
"""
from threading import Timer


results = []


def printer(val):
    """Puts the string representation of the given value into the results
    array.
    """
    results.append(str(val))


def assertion(input):
    """Tests that the given input matches the results array."""
    assert input == results


def scheduler(func, func_arg, delay_ms):
    """Calls the given function with the given argument after the given delay
    time in milliseconds.
    """
    seconds = float(delay_ms) / 1000
    timer = Timer(seconds, func, args=(func_arg,))
    timer.start()


if __name__ == '__main__':
    expected = ['task4', 'task1', 'task3', 'task2']
    total_delay = 5050
    print('running threads, expect results shortly')
    scheduler(printer, 'task1', 300)
    scheduler(printer, 'task2', 5000)
    scheduler(printer, 'task3', 1000)
    scheduler(printer, 'task4', 150)
    scheduler(assertion, expected, total_delay)
