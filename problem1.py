#!/usr/bin/env python3
"""Problem #1 from dailycodingproblem.com

Given a list of numbers and a number, k, return whether any two numbers from
the list sum to k.

Bonus: Do it in one pass.
"""


def list_contains_sum(lst, k):
    """Determines if any two elements of the given list sum to k.

    For example, given lst = [10, 15, 3, 7] and k = 17, return True since
    `10 + 7 == 17`.

    Args:
        lst: The list of numbers to check.
        k: The sum for which to check.

    Returns:
        True if the any two numbers in the list sum to k, False otherwise.
    """
    for x in range(0, len(lst)):
        slice = lst[x + 1:]
        base = lst[x]

        for y in slice:
            if base + y == k:
                return True

    return False


def list_contains_sum_improved(lst, k):
    """Determines if any two elements of the given list sum to k.

    For example, given lst = [10, 15, 3, 7] and k = 17, return True since
    `10 + 7 == 17`.

    This version runs in one pass.

    Args:
        lst: The list of numbers to check.
        k: The sum for which to check.

    Returns:
        True if the any two numbers in the list sum to k, False otherwise.
    """
    for x in lst:
        target = k - x
        if target in lst:
            return True

    return False


if __name__ == '__main__':
    lst = [10, 15, 3, 7]
    good = [25, 22, 18, 17, 13, 10]

    for x in range(0, 27):
        # if x in good and not list_contains_sum(lst, x):
        if x in good and not list_contains_sum_improved(lst, x):
            print('fail on ' + str(x))
