#!/usr/bin/env python3
"""Problem #8 from dailycodingproblem.com

A unival tree (which stands for "universal value") is a tree where all nodes
under it have the same value.

Given the root to a binary tree, count the number of unival subtrees.

For example, the following tree has 5 unival subtrees:
       0
      / \
     1   0
        / \
       1   0
      / \
     1   1
"""


class Node:
    """Represents a node in a binary tree.

    Attributes:
        val: The value of the node.
        left: The left child node. Defaults to `None`
        right: The right child node. Defaults to `None`
    """

    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def count_univals(root):
    """Recursively calculates the number of unival subtrees attached to the
    tree with the given root node.

    Args:
        root: The root node of the tree for which to calculate number of unival
            subtrees.

    Returns:
        The number of unival subtrees. This number will always be gte 0.
    """
    left_val = None if not root.left else root.left.val
    right_val = None if not root.right else root.right.val
    count = 1 if left_val == right_val else 0
    count += count_univals(root.left) if root.left else 0
    count += count_univals(root.right) if root.right else 0

    return count


if __name__ == '__main__':
    tree = Node(
        0,
        Node(1),
        Node(0, Node(
            1,
            Node(1),
            Node(1)
        ), Node(0)))

    assert count_univals(tree) == 5
