#!/usr/bin/env python3
"""Problem #13 from dailycodingproblem.com

This problem was asked by Amazon.

Given an integer k and a string s, find the length of the longest substring
that contains at most k distinct characters.

For example, given s = "abcba" and k = 2, the longest substring with k distinct
characters is "bcb".
"""


def longest_substring_length(orig, max_chars):
    """Determines the length of the longest substring from given original with
    the given number of distinct characters.

    Utilizes a sliding window to inspect contents to limit number of accesses.

    Args:
        orig: The original string.
        max_chars: The max number of distinct characters. It is assumed that
            this value is gte 1.

    Returns:
        The length of the longest candidate substring.
    """
    frame = orig[0:max_chars]
    longest = len(frame)

    for i in range(max_chars, len(orig)):
        frame += orig[i]

        while len(set(frame)) > max_chars:
            frame = frame[1:]

        longest = len(frame) if len(frame) > longest else longest

    return longest


if __name__ == '__main__':
    assert longest_substring_length('abcba', 2) == 3
    assert longest_substring_length('abccba', 2) == 4
    assert longest_substring_length('abccba', 1) == 2
    assert longest_substring_length('abcbaabcbaddds', 3) == 10
