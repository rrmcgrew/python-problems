#!/usr/bin/env python3
"""Problem #3 from dailycodingproblem.com

Given the root to a binary tree, implement serialize(root), which serializes
the tree into a string, and deserialize(s), which deserializes the string back
into the tree.
"""


class Node:
    """Provided class that represents a node in a binary tree."""

    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def serialize(root):
    """Serializes a tree, originating at the given root, into a string.

    The nodes `val` field is used to represent the node, using '&' to
    represent None. The tree is serialized using a depth-first strategy.

    For example, given the following tree:

          1
         / \
        2   3
       / \
      4   5

    The serialization will be '1,2,4,&,&,5,&,&,3,&,&'.

    Args:
        root: The root node for the binary tree to be serialized.

    Returns:
        The string containing the serialized tree.
    """
    def helper(node):
        """Recursive helper method for fetching node values."""
        if not node:
            vals.append('&')
        else:
            vals.append(str(node.val))
            helper(node.left)
            helper(node.right)

    vals = []
    helper(root)

    return ','.join(vals)


def deserialize(data):
    """Deserializes the given data string into a binary tree.

    The data to be deserialized contains the node values for each node,
    separated by commas. The tree is constructed using a depth-first approach.

    Args:
        data: A string containing a serialized representation of a binary tree

    Returns:
        Node representing the root of a binary tree created from the given data
    """
    def helper():
        """Recursive helper method for building nodes."""
        val = next(vals)
        node = None

        if val != '&':
            left = helper()
            right = helper()
            node = Node(val, left, right)

        return node

    vals = iter(data.split(','))
    return helper()


node = Node('root', Node('left', Node('left.left')), Node('right'))
assert deserialize(serialize(node)).left.left.val == 'left.left'
