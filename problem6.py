#!/usr/bin/env python3
"""Problem #6 from dailycodingproblem.com

An XOR linked list is a more memory efficient doubly linked list. Instead of
each node holding next and prev fields, it holds a field named both, which is
an XOR of the next node and the previous node. Implement an XOR linked list; it
has an add(element) which adds the element to the end, and a get(index) which
returns the node at index.

If using a language that has no pointers (such as Python), you can assume you
have access to get_pointer and dereference_pointer functions that converts
between nodes and memory addresses.
"""


class Node:
    """A node for use in a doubly-linked list.

    Attributes:
        npx: The result of bitwise XOR on the previous and next nodes'
            addresses.
    """

    def __init__(self):
        self.npx = 0


class XorLinkedList:
    """Doubly inked list that is more memory efficient than a normal linked
    list.

    Instead of storing references to previous and next node addresses, the
    bitwise XOR of the addresses of previous and next is stored.

    List is initialized with a node. This is a design choice that could easily
    be changed.

    Attributes:
        length: The number of nodes in the list.
        head: The first node in the list.
        _pointers: A dictionary keyed to the memory address values for each
            node. This is a hack for a referencing engine that has an
            obvious negative impact on performance.
    """

    def __init__(self):
        """Initializes the doubly linked list."""
        self.length = 1
        self.head = Node()
        self._pointers = {id(self.head): self.head}

    def get(self, index):
        """Returns the node at the given index. Index values are 0-based.

        Index value must be gte 0 and less than the list length.

        Args:
            index: The index of the node to fetch.

        Raises:
            ValueError: Raised when index is outside the bounds of the list.

        Returns:
            The node found at the given index in the list.
        """
        if 0 > index >= self.length:
            raise ValueError('index out of bounds')

        current = 0
        node = self.head
        previous_address = 0

        while current < index:
            next_address = node.npx ^ previous_address
            previous_address = id(node)
            node = self._pointers[next_address]
            current += 1

        return node

    def add(self, node):
        """Adds the given node to the end of the list.

        Args:
            node: The node to be added to the list.
        """
        tail = self.get(self.length - 1)
        tail.npx = tail.npx ^ id(node)
        node.npx = node.npx ^ id(tail)
        self.length += 1
        self._pointers[id(node)] = node


if __name__ == '__main__':
    xll = XorLinkedList()

    assert xll.length == 1
    assert xll.get(0) is xll.head

    node1 = Node()
    xll.add(node1)

    assert xll.length == 2
    assert node1.npx == id(xll.head)
    assert xll.get(1) is node1

    node2 = Node()
    xll.add(node2)

    assert xll.length == 3
    assert node2.npx == id(node1)
    assert node1.npx == id(xll.head) ^ id(node2)
    assert xll.get(2) is node2
