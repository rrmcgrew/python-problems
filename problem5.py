#!/usr/bin/env python3
"""Problem #5 from dailycodingproblem.com

`cons(a, b)` constructs a pair, and `car(pair)` and `cdr(pair)` returns the
first and last element of that pair. For example, `car(cons(3, 4))` returns 3,
and `cdr(cons(3, 4))` returns 4.

Given this implementation of cons:

    def cons(a, b):
        def pair(f):
            return f(a, b)

        return pair

Implement car and cdr.
"""


def cons(a, b):
    """Provided method to construct a pair."""
    def pair(f):
        return f(a, b)

    return pair


def car(pair):
    """Retrieves the first value of the given pair.

    For example, if a pair (a, b) is passed, a is returned.

    Args:
        pair: The pair from which to fetch the first value.

    Returns:
        The first value of the pair.
    """
    def first(a, b):
        return a

    return pair(first)


def cdr(pair):
    """Retrieves the last value of the given pair.

    For example, if a pair (a, b) is passed, b is returned.

    Args:
        pair: The pair from which to fetch the last value.

    Returns:
        The last value of the pair.
    """
    def last(a, b):
        return b

    return pair(last)


c = cons(3, 4)

assert car(c) == 3
assert cdr(c) == 4
