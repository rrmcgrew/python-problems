#!/usr/bin/env python3
"""Problem #9 from dailycodingproblem.com

Given a list of integers, write a function that returns the largest sum of
non-adjacent numbers. Numbers can be 0 or negative.

For example, [2, 4, 6, 2, 5] should return 13, since we pick 2, 6, and 5.
[5, 1, 1, 5] should return 10, since we pick 5 and 5.

Follow-up: Can you do this in O(N) time and constant space?
"""


def max_sum_non_adjacent(input_list):
    """Defines the maximum sum achievable with no adjacent elements used as
    addends.

    Args:
        input_list: The list/array of integers from which the sum will be
            derived.
    Returns:
        The maximum sum of non-adjacent elements.
    """
    inclusive = input_list[0]
    exclusive = 0

    for i in range(1, len(input_list)):
        # current maximum excluding the current element
        new_exclusive = max(inclusive, exclusive)

        # new inclusive and exclusive sums
        inclusive = exclusive + input_list[i]
        exclusive = new_exclusive

    # return the max of inclusive and exclusive
    return max(inclusive, exclusive)


if __name__ == '__main__':
    input_a = [2, 4, 6, 2, 5]
    input_b = [5, 1, 1, 5]
    input_c = [5, 5, 10, 100, 10, 5]
    input_d = [1, 45, 4]
    input_e = [5, -1, -5, -10, -2, -3]

    assert max_sum_non_adjacent(input_a) == 13
    assert max_sum_non_adjacent(input_b) == 10
    assert max_sum_non_adjacent(input_c) == 110
    assert max_sum_non_adjacent(input_d) == 45
    assert max_sum_non_adjacent(input_e) == 5
