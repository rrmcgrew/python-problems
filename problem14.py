#!/usr/bin/env python3
"""Problem #14 from dailycodingproblem.com

This problem was asked by Google.

The area of a circle is defined as πr^2. Estimate π to 3 decimal places using a
Monte Carlo method.

Hint: The basic equation of a circle is x^2 + y^2 = r^2.
"""
import random
import time


def estimate_pi(places=3):
    """Estimates pi to the given number of decimal places via Monte Carlo
    method.

    Args:
        places: The number of decimal places to which to estimate pi. Defaults
            to 3.
    Returns:
        Float estimation of pi truncated to the given places.
    """
    random.seed(time.time())
    n_in_circle = 0
    total_n = 0
    trunc_factor = 10 ** places

    for _ in range(100000000):
        point = (random.uniform(0, 1), random.uniform(0, 1))
        location = point[0] ** 2 + point[1] ** 2
        total_n += 1
        n_in_circle += 1 if location <= 1 else 0

    result = n_in_circle * 4 / total_n

    return int(result * trunc_factor) / trunc_factor


if __name__ == '__main__':
    print(estimate_pi())
