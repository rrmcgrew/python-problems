#!/usr/bin/env python3
"""Problem #2 from dailycodingproblem.com

Given an array of integers, return a new array such that each element at
index i of the new array is the product of all the numbers in the original
array except the one at i.

For example, if our input was [1, 2, 3, 4, 5], the expected output would be
[120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be
[2, 3, 6].

Follow-up: what if you can't use division?
"""


def get_total_product(lst):
    """Returns the total product considering all items in the given list."""
    total = 1

    for x in lst:
        total = total * x

    return total


def get_products(orig):
    """Creates a new array containing the products as defined above.

    Args:
        orig: The original array.

    Returns:
        The created array.
    """
    total = get_total_product(orig)

    return [int(total / x) for x in orig]


def get_products_sans_division(orig):
    """Creates a new array containing the products as defined above.

    This method doesn't use division.

    Args:
        orig: The original array.

    Returns:
        The created array.
    """
    total = get_total_product(orig)
    new = []

    for x in orig:
        count = 0
        running = total

        while running > 0:
            running = running - x
            count = count + 1

        new.append(count)

    return new


print(str(get_products([1, 2, 3, 4, 5])))
print(str(get_products([3, 2, 1])))
print(str(get_products_sans_division([1, 2, 3, 4, 5])))
print(str(get_products_sans_division([3, 2, 1])))
