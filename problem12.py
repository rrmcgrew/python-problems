#!/usr/bin/env python3
"""Problem #12 from dailycodingproblem.com

This problem was asked by Amazon.
There exists a staircase with N steps, and you can climb up either 1 or 2 steps
at a time. Given N, write a function that returns the number of unique ways you
can climb the staircase. The order of the steps matters.

For example, if N is 4, then there are 5 unique ways:

    1, 1, 1, 1
    2, 1, 1
    1, 2, 1
    1, 1, 2
    2, 2

What if, instead of being able to climb 1 or 2 steps at a time, you could climb
any number from a set of positive integers X? For example, if X = {1, 3, 5},
you could climb 1, 3, or 5 steps at a time.
"""


def ways_to_climb(num_stairs, steps=[1, 2]):
    """Recursively determines the number of ways to climb the given number of
    stairs using the given steps as increments. This assumes that overshooting
    is not possible.

    Args:
        num_stairs: The number of stairs for which ascension methods are to be
            calculated.
        steps: Possible stepping increments.

    Returns:
        A positive integer representing the number of ways to ascend.
    """
    num_ways = 0

    for increment in steps:
        if num_stairs > increment:
            num_ways += ways_to_climb(num_stairs - increment, steps)
        elif num_stairs == increment:
            num_ways += 1

    return num_ways


if __name__ == '__main__':
    assert ways_to_climb(4, steps=[1, 2]) == 5
    assert ways_to_climb(4, steps=[1, 2, 3]) == 7
